from flask import Flask
from flask_restful import Api, Resource

app = Flask(__name__)
api = Api(app)


class Hello(Resource):
    def get(self, name):
        return "Hello " + name


api.add_resource(Hello, "/hello/<string:name>")

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
