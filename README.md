# simple-rest-api-server <a href="https://www.python.org" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg" alt="python" width="40" height="40"/> </a> <a href="https://flask.palletsprojects.com/" target="_blank"> <img src="https://www.vectorlogo.zone/logos/pocoo_flask/pocoo_flask-icon.svg" alt="flask" width="40" height="40"/> </a>  <a href="https://www.docker.com/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/docker/docker-original-wordmark.svg" alt="docker" width="40" height="40"/> </a>

## General info
This project is a very simple rest server made for learning docker, flask and CI.

	
## Technologies
Project is created with:
* python: 3.8.8
* flask: 2.0.1
* docker: 20.10.7
* docker-compose: 1.29.2
	
## Setup
To run app type:

```
docker-compose up simple-rest-api-server
```

If you would like to run tests:

```
docker-compose run -T --rm tests -m pytest -v -r A -k "test_" ./tests/projects/simple_rest_api_server
```